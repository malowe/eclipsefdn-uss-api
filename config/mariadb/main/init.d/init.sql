CREATE DATABASE eclipse_api;
USE eclipse_api;

-- eclipse_api.api_eclipse_api_blob

CREATE TABLE `api_eclipse_api_blob` (
  `bid` SERIAL NOT NULL COMMENT 'Primary key of the blob entity',
  `uid` int(11) unsigned NOT NULL DEFAULT 0 COMMENT 'The user uid',
  `blob_key` varchar(255) NOT NULL COMMENT 'The blob_key value of the blob.',
  `application_token` varchar(255) NOT NULL COMMENT 'The application_token value of the blob.',
  `blob_value` BLOB NOT NULL  COMMENT 'The content of the blob.',
  `etag` varchar(255) NOT NULL COMMENT 'The etag value of the blob.',
  `created` int(11) NOT NULL DEFAULT 0 COMMENT 'The Unix timestamp when the blob was created',
  `changed` int(11) NOT NULL DEFAULT 0 COMMENT 'The Unix timestamp when the blob was most recently saved.',
  UNIQUE KEY `UniqueTokenAndKey` (`application_token`, `blob_key`, `uid`),
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Holds blob data for the Eclipse API.';

-- eclipse_api.api_eclipse_api_blob_application_usage

CREATE TABLE `api_eclipse_api_blob_application_usage` (
  `id` SERIAL NOT NULL COMMENT 'The unique ID for this entry',
  `uid` int(11) unsigned NOT NULL DEFAULT 0 COMMENT 'The user uid',
  `blob_key` varchar(255) NOT NULL COMMENT 'The blob_key value of the blob.',
  `action` varchar(255) NOT NULL COMMENT 'The action the user has taken.',
  `application_token` varchar(255) NOT NULL COMMENT 'The application_token value of the blob.',
  `created` int(11) NOT NULL DEFAULT 0 COMMENT 'The Unix timestamp when the blob was created',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='USS Application data usage.';
