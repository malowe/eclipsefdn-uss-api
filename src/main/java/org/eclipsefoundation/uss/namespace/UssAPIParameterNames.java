/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public class UssAPIParameterNames implements UrlParameterNamespace {
    public static final UrlParameter BLOB_KEY = new UrlParameter("blob_key");
    public static final UrlParameter UID = new UrlParameter("uid");
    public static final UrlParameter APPLICATION_TOKEN = new UrlParameter("application_token");
    public static final UrlParameter ETAG = new UrlParameter("etag");

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(BLOB_KEY, UID, APPLICATION_TOKEN, ETAG));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
