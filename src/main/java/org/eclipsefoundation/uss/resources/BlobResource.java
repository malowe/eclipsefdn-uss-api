/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.resources;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.helpers.BlobHelper;
import org.eclipsefoundation.uss.models.BlobData;
import org.eclipsefoundation.uss.models.BlobUpdateRequest;
import org.eclipsefoundation.uss.models.BlobUrl;
import org.eclipsefoundation.uss.models.mappers.BlobMapper;
import org.eclipsefoundation.uss.services.ApplicationUsageService;
import org.eclipsefoundation.uss.services.BlobService;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("")
@Produces("application/json")
@Consumes("application/json")
public class BlobResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(BlobResource.class);

    private static final String NOT_FOUND_MSG = "The application_token/key pair was not found.";
    private static final String MALFORMED_SYNTAX_MESSAGE = "The request could not be understood by the server due to malformed syntax. The client should not repeat the request without modifications.";

    @Inject
    BlobHelper blobHelper;

    @Inject
    BlobService blobService;
    @Inject
    ApplicationUsageService usageService;

    @Inject
    RequestWrapper wrapper;
    @Inject
    RequestTokenWrapper requestToken;

    @Inject
    BlobMapper mapper;

    @GET
    public Response getBlobs() {
        return getBlobs("");
    }

    @GET
    @Path("/{applicationToken}")
    public Response getBlobs(@PathParam("applicationToken") String token) {
        // Track API usage
        usageService.trackIndexUse(wrapper, token, "");
        return Response.ok(blobService.getBlobs(wrapper, token).stream().map(mapper::toModel).collect(Collectors.toList())).build();
    }

    @GET
    @Path("/{applicationToken}/{blobKey}")
    public Response searchBlob(@PathParam("applicationToken") String token, @PathParam("blobKey") String blobKey) {

        Optional<Blob> result = blobService.getBlob(wrapper, token, blobKey);
        if (result.isEmpty()) {
            LOGGER.error("{}, {}/{} for user '{}'", NOT_FOUND_MSG, token, blobKey, requestToken.getTokenStatus().getUserId());
            throw new NotFoundException(NOT_FOUND_MSG);
        }

        // Track API usage
        usageService.trackRetrieveUse(wrapper, token, blobKey);

        BlobData out = mapper.toModel(result.get());

        // See if the client has provided the required HTTP headers
        if (blobHelper.modifiedAndMatchHeadersAreValid(blobHelper.toHTTPDate(out.getChanged()), out.getEtag())) {

            // All 304 responses must send an etag if the 200 response for the same object contained an etag
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Headers match requested resource");
            }
            return Response
                    .status(Status.NOT_MODIFIED)
                    .header("status", "304 Not Modified")
                    .header(HttpHeaderNames.ETAG, wrapEtagInQuotes(out.getEtag()))
                    .build();
        }

        // Serve Blob from cache
        return serveBlobFromCache(Response.ok(out), out.getChanged(), out.getEtag());
    }

    @DELETE
    @Path("/{applicationToken}/{blobKey}")
    public Response deleteBlob(@PathParam("applicationToken") String token, @PathParam("blobKey") String blobKey) {

        String ifMatch = blobHelper.getHeaderValueWithoutQuotes(HttpHeaderNames.IF_MATCH);
        // check if it exists
        if (blobService.getBlobs(wrapper, token, blobKey, ifMatch).isEmpty()) {
            LOGGER.error("{}, {}/{} for user '{}'", NOT_FOUND_MSG, token, blobKey, requestToken.getTokenStatus().getUserId());
            throw new NotFoundException(NOT_FOUND_MSG);
        }

        // Delete and track API usage
        blobService.deleteBlob(wrapper, token, blobKey);
        usageService.trackDeleteUse(wrapper, token, blobKey);

        return Response.noContent().status(Status.NO_CONTENT).build();
    }

    @PUT
    @Path("/{applicationToken}/{blobKey}")
    public Response updateBlob(@PathParam("applicationToken") String token, @PathParam("blobKey") String blobKey, BlobUpdateRequest body) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Incoming request: /{}/{}, with body: {}", token, blobKey, body);
        }

        // Validate body value by attempting to decode. Return 400 if not decodeable
        if (!blobHelper.isValidBase64(body.getValue())) {
            throw new BadRequestException("Invalid BASE64 string.");
        }

        // If there is no match, we create a new Blob entity
        List<Blob> results = blobService.getBlobs(wrapper, token, blobKey);
        if (results.isEmpty()) {
            return createAndTrackNewBlob(body.getValue(), token, blobKey);
        }

        // Strip quotes from passed header
        String ifMatch = blobHelper.getHeaderValueWithoutQuotes(HttpHeaderNames.IF_MATCH);

        // No If-Match header implies creation but we have results
        if (StringUtils.isBlank(ifMatch)) {
            LOGGER.warn("The resource already exists, etag: {}", results.get(0).getEtag());
            return Response
                    .status(Status.CONFLICT)
                    .header(HttpHeaderNames.ETAG, wrapEtagInQuotes(results.get(0).getEtag()))
                    .entity(Arrays.asList("The resource already exists"))
                    .build();
        }

        // Filter result. Matching against etag
        Optional<Blob> matchesHeader = results.stream().filter(b -> b.getEtag().equals(ifMatch)).findFirst();

        // If-Match header implies it exists. But we don't have a matching etag
        if (matchesHeader.isEmpty()) {
            LOGGER.warn("Invalid/Expired Etag, etag: {}", results.get(0).getEtag());
            return Response
                    .status(Status.CONFLICT)
                    .header(HttpHeaderNames.ETAG, wrapEtagInQuotes(results.get(0).getEtag()))
                    .entity(Arrays.asList("Invalid/Expired Etag"))
                    .build();
        }

        return updateAndTrackExistingBlob(matchesHeader.get(), body.getValue());
    }

    /**
     * Creates a new Blob entity, persists it into the Db, tracks the application usage, and returns a 201 CREATED Response containing the
     * newly created Blob entity URL.
     * 
     * @param blobValue The Blob value.
     * @param appToken The application token
     * @param blobKey The blob key
     * @return A 201 CREATED Response containing the URL to the created entity
     */
    private Response createAndTrackNewBlob(String blobValue, String appToken, String blobKey) {

        // Return a 400 if the persistence fails
        Optional<Blob> createResults = blobService.createNewBlob(wrapper, blobValue, appToken, blobKey);
        if (createResults.isEmpty()) {
            throw new BadRequestException(MALFORMED_SYNTAX_MESSAGE);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Created token/key: {}/{} for user '{}'", appToken, blobKey, requestToken.getTokenStatus().getUserId());
        }

        // Track API use.
        usageService.trackCreateUse(wrapper, appToken, blobKey);

        // Serve from cache. Entity is URL pointed at newly created resource
        ResponseBuilder response = Response
                .status(Status.CREATED)
                .entity(BlobUrl.builder().setUrl(blobHelper.buildEntityUrl(createResults.get().getUid(), appToken, blobKey)).build());
        return serveBlobFromCache(response, createResults.get().getChanged(), createResults.get().getEtag());
    }

    /**
     * Updates a given Blob with a new changed time, blob value, and etag. Persists the Blob, tracks the application usage, and returns a
     * 200 OK with a URL pointing to the realtionship URL.
     * 
     * @param toUpdate The Blob entity to update
     * @param blobValue the new blob value
     * @return A 200 OK Response containing the URL to the updated entity
     */
    private Response updateAndTrackExistingBlob(Blob toUpdate, String blobValue) {

        Optional<Blob> updateResult = blobService.updateBlob(wrapper, toUpdate, blobValue);
        if (updateResult.isEmpty()) {
            throw new BadRequestException(MALFORMED_SYNTAX_MESSAGE);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Updated token/key: {}/{} for user '{}'", updateResult.get().getApplicationToken(),
                            updateResult.get().getBlobKey(), requestToken.getTokenStatus().getUserId());
        }

        // Track API use.
        usageService.trackUpdateUse(wrapper, toUpdate.getApplicationToken(), toUpdate.getBlobKey());

        // Serve from cache. Entity is URL pointed at relationship URL
        ResponseBuilder response = Response
                .ok(BlobUrl
                        .builder()
                        .setUrl(blobHelper
                                .buildEntityUrl(updateResult.get().getUid(), updateResult.get().getApplicationToken(),
                                        updateResult.get().getBlobKey()))
                        .build());
        return serveBlobFromCache(response, toUpdate.getChanged(), toUpdate.getEtag());
    }

    /**
     * Takes a ResponseBuilder object and adds the following headers: Last-Modified, ETag, Expires, and Cache-control. Returns the built
     * Response object.
     * 
     * @param builder Current response to add headers to.
     * @param lastModified The last modified date of the resource.
     * @param etag The ETag of the resource.
     * @return A constructed Response object with the added headers
     */
    private Response serveBlobFromCache(ResponseBuilder builder, int lastModified, String etag) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Adding cache control headers - {}: {}, {}: {}, {}: {}, {}: {}", HttpHeaderNames.LAST_MODIFIED,
                            blobHelper.toHTTPDate(lastModified), HttpHeaderNames.ETAG, etag, HttpHeaderNames.EXPIRES,
                            blobHelper.toHTTPDate(0), HttpHeaderNames.CACHE_CONTROL, "must-revalidate");
        }

        return builder
                .header(HttpHeaderNames.LAST_MODIFIED, blobHelper.toHTTPDate(lastModified))
                .header(HttpHeaderNames.ETAG, wrapEtagInQuotes(etag))
                .header(HttpHeaderNames.EXPIRES, blobHelper.toHTTPDate(0))
                .header(HttpHeaderNames.CACHE_CONTROL, "must-revalidate")
                .build();
    }

    private String wrapEtagInQuotes(String etag) {
        return "\"" + etag + "\"";
    }
}