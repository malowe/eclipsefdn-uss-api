/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.request;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.uss.config.ApplicationTokenFilterConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * This filter is used to validate the {applicationToken} path parameter in incoming requests. If the application token does not match any
 * from a token allow-list, the filter returns a 400 BAD REQUEST.
 */
@Provider
@Priority(10)
public class ApplicationTokenFilter implements ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationTokenFilter.class);

    private static final String APPLICATION_TOKEN = "applicationToken";

    @Inject
    Instance<ApplicationTokenFilterConfig> config;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (config.get().enabled()) {

            String appToken = requestContext.getUriInfo().getPathParameters().getFirst(APPLICATION_TOKEN);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Validating application token: {}", TransformationHelper.formatLog(appToken));
            }
            // Only need to validate application token if it's present in the URL
            if (StringUtils.isNotBlank(appToken)) {

                // Check if it's listed in our allow-list
                if (!config.get().allowedTokens().containsValue(appToken)) {
                    throw new BadRequestException("Invalid application token, please contact the Eclipse Webmaster for a new token.");
                }

                LOGGER.debug("Valid application token");
            }
        }
    }
}
