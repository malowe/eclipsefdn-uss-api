/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.request;

import java.io.IOException;

import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.uss.config.UserAgentFilterConfig;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * This filter is used to validate the User-Agent header attached to incoming requests. If the user agent does not match any from a list of
 * approved agents, the filter returns a 403 FORBIDDEN. This filter should be the first in the request chain.
 */
@Provider
@Priority(1)
public class UserAgentFilter implements ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAgentFilter.class);

    @Inject
    Instance<UserAgentFilterConfig> config;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (config.get().enabled()) {

            String userAgent = requestContext.getHeaderString(HttpHeaderNames.USER_AGENT).trim();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Validating User-Agent header: {}", TransformationHelper.formatLog(userAgent));
            }

            // Forbidden if agent isn't valid or doesn't start with 'uss/'
            if (!config.get().allowedAgents().contains(userAgent) && !userAgent.startsWith("uss/")) {
                LOGGER.error("Invalid User-Agent: {}", userAgent);
                throw new FinalForbiddenException("Invalid User-Agent");
            }
            LOGGER.debug("Valid User-Agent");
        }
    }
}
