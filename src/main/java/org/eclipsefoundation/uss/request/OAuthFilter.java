/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.request;

import java.io.IOException;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.exception.UnauthorizedException;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.namespace.RequestContextPropertyNames;
import org.eclipsefoundation.efservices.services.DrupalOAuthService;
import org.eclipsefoundation.uss.config.OAuthFilterConfig;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is used to validate the incoming Bearer tokens.
 * Sets the current token user in the request context for use downstream.
 */
@Provider
@Priority(5)
public class OAuthFilter implements ContainerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthFilter.class);

    @Inject
    Instance<OAuthFilterConfig> config;

    @Inject
    DrupalOAuthService oauthService;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (config.get().enabled()) {
            LOGGER.debug("All passed headers: {}", requestContext.getHeaders());
            try {
                LOGGER.debug("Parsing auth header");
                String token = DrupalAuthHelper
                        .stripBearerToken(requestContext.getHeaderString(HttpHeaderNames.AUTHORIZATION));

                // Validate read scope if GET request, otherwise it's an update action
                DrupalOAuthData tokenStatus = requestContext.getMethod().equalsIgnoreCase(HttpMethod.GET)
                        ? oauthService.validateTokenStatus(token, config.get().allowedReadScopes(),
                                config.get().allowedClients())
                        : oauthService.validateTokenStatus(token, config.get().allowedWriteScopes(),
                                config.get().allowedClients());

                if (tokenStatus != null && tokenStatus.getUserId() != null) {
                    // Set token info in request context for use downstream
                    requestContext.setProperty(RequestContextPropertyNames.TOKEN_STATUS, tokenStatus);
                } else {
                    LOGGER.error("User cannot be anonymous");
                    throw new UnauthorizedException("User cannot be anonymous");
                }
            } catch (FinalForbiddenException e) {
                throw new UnauthorizedException("Invalid credentials", e);
            }
        }
    }
}
