/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.dtos;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;

/**
 * DTO for tracking operations performed with this application.
 */
@Entity
@Table(name = "api_eclipse_api_blob_application_usage")
public class USSApplicationUsage extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Blob.class, "uau");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int uid;
    private String blobKey;
    private String action;
    private String applicationToken;
    private int created;

    @Override
    public Object getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getBlobKey() {
        return blobKey;
    }

    public void setBlobKey(String blobKey) {
        this.blobKey = blobKey;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getApplicationToken() {
        return applicationToken;
    }

    public void setApplicationToken(String applicationToken) {
        this.applicationToken = applicationToken;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("USSApplicationUsage [id=");
        builder.append(id);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", blob_key=");
        builder.append(blobKey);
        builder.append(", application_token=");
        builder.append(applicationToken);
        builder.append(", action=");
        builder.append(action);
        builder.append(", created=");
        builder.append(created);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, uid, blobKey, applicationToken, action, created);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        USSApplicationUsage other = (USSApplicationUsage) obj;
        return Objects.equals(id, other.id) && Objects.equals(uid, other.uid)
                && Objects.equals(blobKey, other.blobKey) && Objects.equals(applicationToken, other.applicationToken)
                && Objects.equals(action, other.action) && Objects.equals(created, other.created);
    }

    @Singleton
    public static class USSApplicationUsageFilter implements DtoFilter<USSApplicationUsage> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            if (isRoot) {
                String applicationToken = params.getFirst(UssAPIParameterNames.APPLICATION_TOKEN.getName());
                if (StringUtils.isNotEmpty(applicationToken)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".applicationToken = ?",
                                    new Object[] { applicationToken }));
                }

                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                    new Object[] { Long.valueOf(id) }));
                }

                String blobKey = params.getFirst(UssAPIParameterNames.BLOB_KEY.getName());
                if (StringUtils.isNotEmpty(blobKey)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".blobKey = ?",
                                    new Object[] { blobKey }));
                }

                String uid = params.getFirst(UssAPIParameterNames.UID.getName());
                if (StringUtils.isNumeric(uid)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                                    new Object[] { Integer.valueOf(uid) }));
                }
            }

            return statement;
        }

        @Override
        public Class<USSApplicationUsage> getType() {
            return USSApplicationUsage.class;
        }
    }
}
