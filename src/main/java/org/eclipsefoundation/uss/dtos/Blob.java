/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.dtos;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Blob DTO
 */
@Entity
@Table(name = "api_eclipse_api_blob", uniqueConstraints = {
        @UniqueConstraint(name = "UniqueTokenAndKey", columnNames = { "blob_key", "application_token", "uid" }) })
public class Blob extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Blob.class, "blob");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bid;
    private int uid;

    @Column(name = "blob_key")
    private String blobKey;
    @Column(name = "application_token")
    private String applicationToken;
    private String etag;
    private int created;
    private int changed;

    @Column(columnDefinition = "BLOB")
    private byte[] blobValue;

    @JsonIgnore
    @Override
    public Object getId() {
        return getBid();
    }

    public long getBid() {
        return this.bid;
    }

    public void setBid(long bid) {
        this.bid = bid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getBlobKey() {
        return blobKey;
    }

    public void setBlobKey(String blobKey) {
        this.blobKey = blobKey;
    }

    public String getApplicationToken() {
        return applicationToken;
    }

    public void setApplicationToken(String applicationToken) {
        this.applicationToken = applicationToken;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getChanged() {
        return changed;
    }

    public void setChanged(int changed) {
        this.changed = changed;
    }

    public byte[] getBlobValue() {
        return blobValue;
    }

    public void setBlobValue(byte[] blobValue) {
        this.blobValue = blobValue;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Blob [bid=");
        builder.append(bid);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", blob_key=");
        builder.append(blobKey);
        builder.append(", application_token=");
        builder.append(applicationToken);
        builder.append(", etag=");
        builder.append(etag);
        builder.append(", created=");
        builder.append(created);
        builder.append(", changed=");
        builder.append(changed);
        builder.append(", blob_value=");
        builder.append(blobValue);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(bid, uid, blobKey, applicationToken, etag, created, changed, blobValue);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Blob other = (Blob) obj;
        return Objects.equals(bid, other.bid) && Objects.equals(uid, other.uid)
                && Objects.equals(blobKey, other.blobKey) && Objects.equals(applicationToken, other.applicationToken)
                && Objects.equals(etag, other.etag) && Objects.equals(created, other.created)
                && Objects.equals(changed, other.changed) && Objects.equals(blobValue, other.blobValue);
    }

    @Singleton
    public static class BlobFilter implements DtoFilter<Blob> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            if (isRoot) {
                String uid = params.getFirst(UssAPIParameterNames.UID.getName());
                if (StringUtils.isNumeric(uid)) {
                    statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                            new Object[] { Integer.valueOf(uid) }));
                }

                String blobKey = params.getFirst(UssAPIParameterNames.BLOB_KEY.getName());
                if (StringUtils.isNotEmpty(blobKey)) {
                    statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".blobKey = ?",
                            new Object[] { blobKey }));
                }

                String applicationToken = params.getFirst(UssAPIParameterNames.APPLICATION_TOKEN.getName());
                if (StringUtils.isNotEmpty(applicationToken)) {
                    statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".applicationToken = ?",
                            new Object[] { applicationToken }));
                }

                String etag = params.getFirst(UssAPIParameterNames.ETAG.getName());
                if (StringUtils.isNotEmpty(etag)) {
                    statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".etag = ?",
                            new Object[] { etag }));
                }
            }

            return statement;
        }

        @Override
        public Class<Blob> getType() {
            return Blob.class;
        }
    }
}
