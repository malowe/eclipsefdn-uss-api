/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class used to perform various checks and create various entities used by the application.
 */
@ApplicationScoped
public final class BlobHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(BlobHelper.class);

    @ConfigProperty(name = "eclipse.uss.blob.entity-url-format", defaultValue = "https://api.eclipse.org/account/profile/%d/blob/%s/%s")
    String entityUrlFormat;

    @Inject
    RequestTokenWrapper requestToken;
    @Inject
    RequestWrapper wrapper;

    /**
     * Attempts to decode a Base64 String and returns whether it was successful. It won't succeed if the String is not valid Base64
     * 
     * @param src The Base64 encoded String.
     * @return True if valid base64 String, false if not
     */
    public boolean isValidBase64(String src) {
        try {
            Base64.getDecoder().decode(src);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error decoding base64 string: {}", src, e);
            return false;
        }
    }

    /**
     * Builds the URL pointing to the profile relationship URL.
     * 
     * @param token The given application token.
     * @param key The given blob key
     * @return A URL containing the location of a resource.
     */
    public String buildEntityUrl(int uid, String token, String key) {
        return String.format(entityUrlFormat, uid, token, key);
    }

    /**
     * Converts a unix timestamp in seconds to an HTTP Date formatted string. Ex: Thu, 04 May 2023 14:36:09 GMT
     * 
     * @param timeInSeconds The timestamp in seconds to convert
     * @return An HTTP formatted date string
     */
    public String toHTTPDate(int timeInSeconds) {
        DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        long timeStamp = TimeUnit.SECONDS.toMillis(timeInSeconds);
        return format.format(new Date(timeStamp)) + " GMT";
    }

    /**
     * Used to validate that the client sent the required headers for a retrieve, and that they match the requested resource.
     * 
     * @param lastModified The last modified date of the resource.
     * @param etag The requested resource's ETAG
     * @return True if the headers are valid
     */
    public boolean modifiedAndMatchHeadersAreValid(String lastModified, String etag) {
        String ifModifiedSince = getHeaderValueWithoutQuotes(HttpHeaderNames.IF_MODIFIED_SINCE);
        String ifNoneMatch = getHeaderValueWithoutQuotes(HttpHeaderNames.IF_NONE_MATCH);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Validating header If-None-Match: {} against ETag: {}", ifNoneMatch, etag);
            LOGGER.debug("Validating header If-Modified-Since: {} against lastModified: {}", ifModifiedSince, lastModified);
        }
        return StringUtils.isNotBlank(ifNoneMatch) && ifNoneMatch.equals(etag)
                && (StringUtils.isBlank(ifModifiedSince) || ifModifiedSince.equals(lastModified));
    }

    /**
     * Builds a MultivaluedMap containing the desired search params. Used to query for Blob entities.
     * 
     * @param token The application token.
     * @param key The blob key.
     * @param etag The blob etag.
     * @return A populated MultivaluedMap entity containing the desired search params.
     */
    public MultivaluedMap<String, String> buildBlobSearchParams(String token, String key, String etag) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(UssAPIParameterNames.UID.getName(), requestToken.getTokenStatus().getUserId());

        if (StringUtils.isNotEmpty(token)) {
            params.add(UssAPIParameterNames.APPLICATION_TOKEN.getName(), token);
        }
        if (StringUtils.isNotEmpty(key)) {
            params.add(UssAPIParameterNames.BLOB_KEY.getName(), key);
        }
        if (StringUtils.isNotEmpty(etag)) {
            params.add(UssAPIParameterNames.ETAG.getName(), etag);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Built search params: {}", params);
        }

        return params;
    }

    /**
     * Instantiates a new Blob entity and populates it with the given values.
     * 
     * @param blobValue The value of the config file to store.
     * @param appToken The application token.
     * @param blobKey The blob key.
     * @return A fully populated Blob entity.
     */
    public Blob buildNewBlob(String blobValue, String appToken, String blobKey) {
        int now = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

        // Create new blob to add to DB
        Blob out = new Blob();
        out.setApplicationToken(appToken);
        out.setBlobKey(blobKey);
        out.setBlobValue(blobValue.getBytes());
        out.setUid(Integer.valueOf(requestToken.getTokenStatus().getUserId()));
        out.setEtag(generateEtag(blobValue, appToken, blobKey));
        out.setChanged(now);
        out.setCreated(now);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Blob created: {}", out);
        }
        return out;
    }

    /**
     * Generates an etag by creating a SHA-256 hash using the blob, app token, and key.
     * 
     * @param blobValue The given blob value
     * @param appToken The given application token
     * @param blobKey The given blob key
     * @return A SHA-256 hex-string using the blob, token, and key
     */
    public String generateEtag(String blobValue, String appToken, String blobKey) {
        String input = blobValue + appToken + blobKey;
        String etag = DigestUtils.sha256Hex(input);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Generated Etag: {}", etag);
        }
        return etag;
    }

    /**
     * Gets a header value using the given key and strips away any quotes. This allows proper comparison of etag values.
     * 
     * @param headerKey The desired header key
     */
    public String getHeaderValueWithoutQuotes(String headerKey) {
        String headerValue = wrapper.getHeader(headerKey);
        return StringUtils.isNotBlank(headerValue) ? headerValue.replaceAll("\"*", "") : headerValue;
    }
}
