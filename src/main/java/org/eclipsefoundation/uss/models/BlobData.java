/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Outgoing Blob entity for all read operations
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_BlobData.Builder.class)
public abstract class BlobData {

    public abstract String getApplicationToken();

    public abstract String getEtag();

    public abstract int getChanged();

    public abstract String getKey();

    public abstract String getValue();

    public abstract String getUrl();

    public static Builder builder() {
        return new AutoValue_BlobData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setApplicationToken(String token);

        public abstract Builder setEtag(String etag);

        public abstract Builder setChanged(int changed);

        public abstract Builder setKey(String key);

        public abstract Builder setValue(String value);

        public abstract Builder setUrl(String url);

        public abstract BlobData build();
    }
}