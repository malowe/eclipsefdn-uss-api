/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Outgoing Response body pointing to relationship URL for the given blob.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_BlobUrl.Builder.class)
public abstract class BlobUrl {

    public abstract String getUrl();

    public static Builder builder() {
        return new AutoValue_BlobUrl.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setUrl(String url);

        public abstract BlobUrl build();
    }
}
