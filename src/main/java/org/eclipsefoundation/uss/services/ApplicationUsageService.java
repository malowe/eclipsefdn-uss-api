/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.services;

import java.util.Optional;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.uss.dtos.USSApplicationUsage;

/**
 * Defines the ApplicationUsageService. Provides various methods for tracking
 * the actions taken by the client.
 */
public interface ApplicationUsageService {

    /**
     * Tracks the usage of the index endpoints by persisting a USSAplicationUsage
     * entity into the DB. Stores application_token, blob_key, uid, action, and
     * timestamp.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     * @return An Optional containing a newly created USSApplicationUsage entity
     */
    Optional<USSApplicationUsage> trackIndexUse(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Tracks the usage of the retrieve endpoint by persisting a USSAplicationUsage
     * entity into the DB. Stores application_token, blob_key, uid, action, and
     * timestamp.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     * @return An Optional containing a newly created USSApplicationUsage entity
     */
    Optional<USSApplicationUsage> trackRetrieveUse(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Tracks the usage of the delete endpoint by persisting a USSAplicationUsage
     * entity into the DB. Stores application_token, blob_key, uid, action, and
     * timestamp.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     * @return An Optional containing a newly created USSApplicationUsage entity
     */
    Optional<USSApplicationUsage> trackDeleteUse(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Tracks the usage of the creation endpoint by persisting a USSAplicationUsage
     * entity into the DB. Stores application_token, blob_key, uid, action, and
     * timestamp.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     * @return An Optional containing a newly created USSApplicationUsage entity
     */
    Optional<USSApplicationUsage> trackCreateUse(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Tracks the usage of the update endpoint by persisting a USSAplicationUsage
     * entity into the DB. Stores application_token, blob_key, uid, action, and
     * timestamp.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     * @return An Optional containing a newly created USSApplicationUsage entity
     */
    Optional<USSApplicationUsage> trackUpdateUse(RequestWrapper wrapper, String appToken, String blobKey);
}
