/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.uss.dtos.Blob;
import org.eclipsefoundation.uss.helpers.BlobHelper;
import org.eclipsefoundation.uss.namespace.UssAPIParameterNames;
import org.eclipsefoundation.uss.services.BlobService;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default BlobService implementation. Used to CRUD operations on Blob entities.
 */
@ApplicationScoped
public class DefaultBlobService implements BlobService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultBlobService.class);

    @Inject
    BlobHelper blobHelper;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Override
    public List<Blob> getBlobs(RequestWrapper wrapper, String appToken) {
        return getBlobs(wrapper, appToken, null, null);
    }

    @Override
    public List<Blob> getBlobs(RequestWrapper wrapper, String appToken, String blobKey) {
        return getBlobs(wrapper, appToken, blobKey, null);
    }

    @Override
    public List<Blob> getBlobs(RequestWrapper wrapper, String appToken, String blobKey, String etag) {
        return fetchBlobsFromDB(wrapper, blobHelper.buildBlobSearchParams(appToken, blobKey, etag));
    }

    @Override
    public Optional<Blob> getBlob(RequestWrapper wrapper, String appToken, String blobKey) {

        // Return empty if there are no results
        List<Blob> results = fetchBlobsFromDB(wrapper, blobHelper.buildBlobSearchParams(appToken, blobKey, null));
        if (results.isEmpty()) {
            return Optional.empty();
        }

        String ifNoneMatch = blobHelper.getHeaderValueWithoutQuotes(HttpHeaderNames.IF_NONE_MATCH);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Checking if Blob Etags match {}", ifNoneMatch);
        }

        // Attempt to match against the If-None-Match header
        Optional<Blob> matchesHeader = results.stream().filter(b -> b.getEtag().equals(ifNoneMatch)).findFirst();

        // Serve the Entity matching the header, otherwise default to the first
        if (matchesHeader.isPresent()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found match: {}", matchesHeader);
            }

            return matchesHeader;
        }

        Optional<Blob> defaultResult = Optional.of(results.get(0));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Defaulting to first result: {}", defaultResult);
        }

        return defaultResult;
    }

    @Override
    public Optional<Blob> createNewBlob(RequestWrapper wrapper, String blobValue, String appToken, String blobKey) {
        // Persist Blob
        return Optional.of(persistBlob(wrapper, blobHelper.buildNewBlob(blobValue, appToken, blobKey)));
    }

    @Override
    public Optional<Blob> updateBlob(RequestWrapper wrapper, Blob toUpdate, String blobValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Updating existing Blob: {}", toUpdate);
        }

        // Set blob value, changed time, and generate a new etag
        toUpdate.setChanged((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        toUpdate.setBlobValue(blobValue.getBytes());
        toUpdate.setEtag(blobHelper.generateEtag(blobValue, toUpdate.getApplicationToken(), toUpdate.getBlobKey()));

        return Optional.of(persistBlob(wrapper, toUpdate));
    }

    @Override
    public void deleteBlob(RequestWrapper wrapper, String appToken, String blobKey) {
        String ifMatch = blobHelper.getHeaderValueWithoutQuotes(HttpHeaderNames.IF_MATCH);
        deleteBlobFromDB(wrapper, blobHelper.buildBlobSearchParams(appToken, blobKey, ifMatch));
    }

    /**
     * Fetches all Blob entities in the DB that match the given set of params. A flag can be set to allow the blob value to be included in
     * the return
     * 
     * @param wrapper the request wrapper
     * @param params the set of fields to use when querying the DB.
     * @return A List of Blob entities if they can be found.
     */
    private List<Blob> fetchBlobsFromDB(RequestWrapper wrapper, MultivaluedMap<String, String> params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching all Blobs with params: {}", params);
        }

        List<Blob> results = dao.get(new RDBMSQuery<>(wrapper, filters.get(Blob.class), params));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Found {} result(s)", results.size());
        }

        // Only return blob value when fetching single record
        if (StringUtils.isBlank(params.getFirst(UssAPIParameterNames.APPLICATION_TOKEN.getName()))
                && StringUtils.isBlank(params.getFirst(UssAPIParameterNames.BLOB_KEY.getName()))) {
            results.stream().forEach(b -> b.setBlobValue(new byte[] {}));
        }

        return results;
    }

    /**
     * Deletes a Blob entity from the Db given the desired params.
     * 
     * @param wrapper The request wrapper
     * @param params The set of fields to use when querying the DB
     */
    private void deleteBlobFromDB(RequestWrapper wrapper, MultivaluedMap<String, String> params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleting Blob with params: {}", params);
        }
        dao.delete(new RDBMSQuery<>(wrapper, filters.get(Blob.class), params));
    }

    /**
     * Attempts to persist a Blob entity in the DB.
     * 
     * @param wrapper the request wrapper
     * @param src The Blob entity to persist.
     * @return The persisted Blob if it exists
     */
    private Blob persistBlob(RequestWrapper wrapper, Blob src) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Persisting Blob: {}", src);
        }
        return dao.add(new RDBMSQuery<>(wrapper, filters.get(Blob.class)), Arrays.asList(src)).get(0);
    }
}
