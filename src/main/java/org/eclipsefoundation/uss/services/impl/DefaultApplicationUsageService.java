/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.services.impl;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.models.RequestTokenWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.uss.dtos.USSApplicationUsage;
import org.eclipsefoundation.uss.services.ApplicationUsageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the ApplicationUsageService. Used to track various actions taken by clients.
 */
@ApplicationScoped
public class DefaultApplicationUsageService implements ApplicationUsageService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultApplicationUsageService.class);

    @Inject
    RequestTokenWrapper requestToken;

    @Inject
    PersistenceDao dao;
    @Inject
    FilterService filters;

    @Override
    public Optional<USSApplicationUsage> trackIndexUse(RequestWrapper wrapper, String appToken, String blobKey) {
        return Optional.of(persistUsage(wrapper, buildUsageEntity(appToken, blobKey, "index")));
    }

    @Override
    public Optional<USSApplicationUsage> trackRetrieveUse(RequestWrapper wrapper, String appToken, String blobKey) {
        return Optional.of(persistUsage(wrapper, buildUsageEntity(appToken, blobKey, "retrieve")));
    }

    @Override
    public Optional<USSApplicationUsage> trackDeleteUse(RequestWrapper wrapper, String appToken, String blobKey) {
        return Optional.of(persistUsage(wrapper, buildUsageEntity(appToken, blobKey, "delete")));
    }

    @Override
    public Optional<USSApplicationUsage> trackCreateUse(RequestWrapper wrapper, String appToken, String blobKey) {
        return Optional.of(persistUsage(wrapper, buildUsageEntity(appToken, blobKey, "create")));
    }

    @Override
    public Optional<USSApplicationUsage> trackUpdateUse(RequestWrapper wrapper, String appToken, String blobKey) {
        return Optional.of(persistUsage(wrapper, buildUsageEntity(appToken, blobKey, "update")));
    }

    /**
     * Attempts to persist a USSAplicationUsage entity in the DB. Returns the newly created entity if it exists.
     * 
     * @param wrapper the request wrapper
     * @param src The USSAplicationUsage entity to persist
     * @return The newly created entity if it exists.
     */
    private USSApplicationUsage persistUsage(RequestWrapper wrapper, USSApplicationUsage src) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Persisting USSApplicationUsage: {}", src);
        }
        return dao.add(new RDBMSQuery<>(wrapper, filters.get(USSApplicationUsage.class)), Arrays.asList(src)).get(0);
    }

    /**
     * Builds a USSAplicationUsage entity with the desired parameters. These entities are used to track the application usage.
     * 
     * @param appToken The application token field in the current request.
     * @param key The blob key field in the current request.
     * @param action The current action being taken. Ex: "create", "retrieve"
     * @return A USSAplicationUsage entity.
     */
    private USSApplicationUsage buildUsageEntity(String appToken, String key, String action) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Building USSApplicationUsage with action: {}", action);
        }
        USSApplicationUsage usage = new USSApplicationUsage();
        usage.setAction(action);
        usage.setApplicationToken(appToken);
        usage.setCreated((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        usage.setUid(Integer.valueOf(requestToken.getTokenStatus().getUserId()));
        usage.setBlobKey(key);
        return usage;
    }
}
