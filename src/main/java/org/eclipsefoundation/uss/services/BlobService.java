/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.services;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.uss.dtos.Blob;

/**
 * Defines the BlobService. Provides various methods for fetching, adding, and
 * removing Blobs from the DB.
 */
public interface BlobService {

    /**
     * Fetches a list of Blob entities that match the given paramters. May return an
     * empty list.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token
     * @return A List of Blob entities if they exist. Or an empty list.
     */
    List<Blob> getBlobs(RequestWrapper wrapper, String appToken);

    /**
     * Fetches a list of Blob entities that match the given paramters. May return an
     * empty list.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token
     * @param blobKey  The given blob key
     * @return A List of Blob entities if they exist. Or an empty list.
     */
    List<Blob> getBlobs(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Fetches a list of Blob entities that match the given paramters. May return an
     * empty list.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token
     * @param blobKey  The given blob key
     * @param etag     The given etag.
     * @return A List of Blob entities if they exist. Or an empty list.
     */
    List<Blob> getBlobs(RequestWrapper wrapper, String appToken, String blobKey, String etag);

    /**
     * Fetches a Blob that matches the given paramters. Serves the Entity matching
     * the If-None-Match header, otherwise default to the first result found.
     * 
     * @param wrapper  The current RequestWrapper.
     * @param appToken The given application token
     * @param blobKey  The given blob key
     * @return An Optional containing the desired Blob entity if it exists
     */
    Optional<Blob> getBlob(RequestWrapper wrapper, String appToken, String blobKey);

    /**
     * Creates a new Blob entity and persists it into the Db.
     * 
     * @param blobValue The Blob value.
     * @param appToken  The application token
     * @param blobKey   The blob key
     * @return An optional containing the created entity if it exists
     */
    Optional<Blob> createNewBlob(RequestWrapper wrapper, String blobValue, String appToken, String blobKey);

    /**
     * Updates an existing Blob with a new blob value, change time, and etag.
     * 
     * @param wrapper   The current RequestWrapper.
     * @param toUpdate  The Blob entity to update.
     * @param blobValue The new blob value
     * @return An optional containing the newly update Blob entity if it exists
     */
    Optional<Blob> updateBlob(RequestWrapper wrapper, Blob toUpdate, String blobValue);

    /**
     * Attempts to delete any Blob entities that match the given params.
     * 
     * @param wrapper  The current RequestWrapper
     * @param appToken The given application token.
     * @param blobKey  The given blob key.
     */
    void deleteBlob(RequestWrapper wrapper, String appToken, String blobKey);
}
