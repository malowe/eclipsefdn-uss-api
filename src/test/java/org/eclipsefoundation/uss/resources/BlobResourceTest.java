/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.resources;

import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.eclipsefoundation.uss.models.BlobUpdateRequest;
import org.eclipsefoundation.uss.test.helpers.SchemaNamespaceHelper;
import org.jboss.resteasy.util.HttpHeaderNames;
import org.jose4j.base64url.Base64;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class BlobResourceTest {

    private final static String BASE_URL = "";
    private final static String TOKEN_URL = "/{applicationToken}";
    private final static String FULL_URL = TOKEN_URL + "/{blobKey}";

    private final static String VALID_TEST_APP_TOKEN = "testToken";
    private final static String VALID_TEST_BLOB_KEY = "testKey";
    private final static String VALID_ECLIPSE_APP_TOKEN = "eclipseToken";
    private final static String VALID_ECLIPSE_BLOB_KEY = "eclipseKey";
    private static final String INVALID_APP_TOKEN = "invalid";
    private static final String INVALID_BLOB_KEY = "invalid";
    private static final String DELETABLE_BLOB_KEY = "application.properties";
    private static final String UPDATABLE_BLOB_KEY = "filename.txt";

    private final static Optional<Map<String, Object>> validReadWithAgent = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validRead", HttpHeaderNames.USER_AGENT, "testAgent"));
    private final static Optional<Map<String, Object>> validWriteWithAgent = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validWrite", HttpHeaderNames.USER_AGENT, "validAgent"));
    private final static Optional<Map<String, Object>> validWriteWithEtag = Optional
            .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validWrite", HttpHeaderNames.USER_AGENT, "testAgent",
                    HttpHeaderNames.IF_MATCH, "etag"));

    /*
     * NO PARAMS
     */
    private final static EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.BLOBS_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(validReadWithAgent)
            .build();

    /*
     * /{applicationToken}
     */
    private final static EndpointTestCase GET_ALL_BY_TOKEN_SUCCESS = TestCaseHelper
            .prepareTestCase(TOKEN_URL, new String[] { VALID_TEST_APP_TOKEN }, SchemaNamespaceHelper.BLOBS_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(validReadWithAgent)
            .build();

    private final static EndpointTestCase GET_ALL_BY_TOKEN_INVALID_APP_TOKEN = TestCaseHelper
            .prepareTestCase(TOKEN_URL, new String[] { INVALID_APP_TOKEN }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400)
            .setHeaderParams(validReadWithAgent)
            .build();

    /*
     * /{applicationToken}/{blobKey}
     */
    private final static EndpointTestCase GET_SINGLE_SUCCESS = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.BLOB_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(validReadWithAgent)
            .build();

    private final static EndpointTestCase READ_SINGLE_INVALID_APP_TOKEN = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { INVALID_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400)
            .setHeaderParams(validReadWithAgent)
            .build();

    private final static EndpointTestCase UPDATE_SUCCESS = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, UPDATABLE_BLOB_KEY },
                    SchemaNamespaceHelper.BLOB_URL_SCHEMA_PATH)
            .setStatusCode(200)
            .setHeaderParams(validWriteWithEtag)
            .build();

    private final static EndpointTestCase CREATE_CONFLICT = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(409)
            .setHeaderParams(validWriteWithAgent)
            .build();

    private final static EndpointTestCase UPDATE_CONFLICT = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(409)
            .setHeaderParams(validWriteWithEtag)
            .build();

    private final static EndpointTestCase WRITE_SINGLE_BAD_REQUEST = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400)
            .setHeaderParams(validWriteWithAgent)
            .build();

    private final static EndpointTestCase WRITE_SINGLE_INVALID_APP_TOKEN = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { INVALID_APP_TOKEN, VALID_TEST_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400)
            .setHeaderParams(validWriteWithAgent)
            .build();

    private final static EndpointTestCase NOT_FOUND_READ_CASE = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, INVALID_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(validReadWithAgent)
            .setStatusCode(404)
            .build();

    private final static EndpointTestCase NOT_FOUND_DELETE_BAD_KEY = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, INVALID_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(validWriteWithAgent)
            .setStatusCode(404)
            .build();

    private final static EndpointTestCase NOT_FOUND_DELETE_BAD_ETAG = TestCaseHelper
            .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, VALID_ECLIPSE_BLOB_KEY },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setHeaderParams(validWriteWithEtag)
            .setStatusCode(404)
            .build();

    @Inject
    ObjectMapper objectMapper;

    /*
     * GET /
     */
    @Test
    void testGetAll_success() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void testGetAll_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetAll_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetAll_failure_badAuth() {
        EndpointTestBuilder.from(buildInvalidAuthCase(BASE_URL, new String[] {})).run();
    }

    @Test
    void testGetAll_failure_invalidUserAgent() {
        EndpointTestBuilder.from(buildReadInvalidUserAgentCase(BASE_URL, new String[] {})).run();
    }

    /*
     * GET /{applicationToken}
     */
    @Test
    void testGetAllByToken_success() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_SUCCESS).run();
    }

    @Test
    void testGetAllByToken_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetAllByToken_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetAllByToken_failure_badAuth() {
        EndpointTestBuilder.from(buildInvalidAuthCase(TOKEN_URL, new String[] { VALID_TEST_APP_TOKEN })).run();
    }

    @Test
    void testGetAllByToken_failure_invalidUserAgent() {
        EndpointTestBuilder.from(buildReadInvalidUserAgentCase(TOKEN_URL, new String[] { VALID_TEST_APP_TOKEN })).run();
    }

    @Test
    void testGetAllByToken_failure_badRequest_invalidAppToken() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_INVALID_APP_TOKEN).run();
    }

    @Test
    void testGetAllByToken_failure_badRequest_invalidAppToken_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_INVALID_APP_TOKEN).andCheckSchema().run();
    }

    @Test
    void testGetAllByToken_failure_badRequest_invalidAppToken_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_BY_TOKEN_INVALID_APP_TOKEN).andCheckFormat().run();
    }

    /*
     * GET /{applicationToken}/{blobKey}
     */
    @Test
    void testGetSingle_success() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).run();
    }

    @Test
    void testGetSingle_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetSingle_success_validateSchema() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetSingle_success_notModified() {
        // Send header with matching Etag
        Optional<Map<String, Object>> headers = Optional
                .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validRead",
                        HttpHeaderNames.USER_AGENT, "testAgent",
                        HttpHeaderNames.IF_NONE_MATCH, "eTaG30Test"));
        EndpointTestBuilder.from(TestCaseHelper
                .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, VALID_ECLIPSE_BLOB_KEY }, BASE_URL)
                .setStatusCode(304)
                .setHeaderParams(headers).build()).run();

        // Send headers with matching Etag and matching modified-since date
        headers = Optional.of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validRead",
                HttpHeaderNames.USER_AGENT, "testAgent",
                HttpHeaderNames.IF_NONE_MATCH, "eTaG30Test",
                HttpHeaderNames.IF_MODIFIED_SINCE, "Fri, 19 May 2023 15:19:28 GMT"));
        EndpointTestBuilder.from(TestCaseHelper
                .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, VALID_ECLIPSE_BLOB_KEY }, BASE_URL)
                .setStatusCode(304)
                .setHeaderParams(headers).build()).run();
    }

    @Test
    void testGetSingle_failure_notFound() {
        EndpointTestBuilder.from(NOT_FOUND_READ_CASE).run();
    }

    @Test
    void testGetSingle_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(NOT_FOUND_READ_CASE).andCheckSchema().run();
    }

    @Test
    void testGetSingle_failure_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(NOT_FOUND_READ_CASE).andCheckFormat().run();
    }

    @Test
    void testGetSingle_failure_badRequest_invalidAppToken() {
        EndpointTestBuilder.from(READ_SINGLE_INVALID_APP_TOKEN).run();
    }

    @Test
    void testGetSingle_failure_badRequest_invalidAppToken_validateSchema() {
        EndpointTestBuilder.from(READ_SINGLE_INVALID_APP_TOKEN).andCheckSchema().run();
    }

    @Test
    void testGetSingle_failure_badRequest_invalidAppToken_validateResponseFormat() {
        EndpointTestBuilder.from(READ_SINGLE_INVALID_APP_TOKEN).andCheckFormat().run();
    }

    @Test
    void testGetSingle_failure_badAuth() {
        EndpointTestBuilder.from(
                buildInvalidAuthCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY })).run();
    }

    @Test
    void testGetSingle_failure_invalidUserAgent() {
        EndpointTestBuilder.from(buildReadInvalidUserAgentCase(FULL_URL,
                new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY })).run();
    }

    /*
     * DELETE /{applicationToken}/{blobKey}
     */
    @Test
    void testDelete_success() {
        EndpointTestBuilder.from(TestCaseHelper
                .prepareTestCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, DELETABLE_BLOB_KEY },
                        SchemaNamespaceHelper.BLOB_SCHEMA_PATH)
                .setStatusCode(204)
                .setHeaderParams(validWriteWithAgent)
                .build()).doDelete(null).run();
    }

    @Test
    void testDelete_failure_notFound() {
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_KEY).doDelete(null).run();
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_ETAG).doDelete(null).run();
    }

    @Test
    void testDelete_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_KEY).doDelete(null).andCheckSchema().run();
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_ETAG).doDelete(null).andCheckSchema().run();
    }

    @Test
    void testDelete_failure_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_KEY).doDelete(null).andCheckFormat().run();
        EndpointTestBuilder.from(NOT_FOUND_DELETE_BAD_ETAG).doDelete(null).andCheckFormat().run();
    }

    @Test
    void testDelete_failure_badAuth() {
        EndpointTestBuilder.from(
                buildInvalidAuthCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, VALID_ECLIPSE_BLOB_KEY }))
                .doDelete(null).run();
    }

    @Test
    void testDelete_failure_invalidUserAgent() {
        EndpointTestBuilder.from(buildWriteInvalidUserAgentCase(FULL_URL)).doDelete(null).run();
    }

    @Test
    void testDelete_failure_badRequest_invalidAppToken() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doDelete(null).run();
    }

    @Test
    void testDelete_failure_badRequest_invalidAppToken_validateSchema() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doDelete(null).andCheckSchema().run();
    }

    @Test
    void testDelete_failure_badRequest_invalidAppToken_validateResponseFormat() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doDelete(null).andCheckFormat().run();
    }

    /*
     * PUT /{applicationToken}/{blobKey}
     */
    @Test
    void testPut_success_create() {
        EndpointTestBuilder.from(buildBlobCreateCase("newfile.txt")).doPut(buildValidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testput_success_create_validateResponseFormat() {
        // Need to use new key value to avoid collision with other create actions
        EndpointTestBuilder.from(buildBlobCreateCase("newerfile.txt")).doPut(
                buildValidBlobUpdateRequestAsJson()).andCheckFormat().run();
    }

    @Test
    void testPut_success_create_validateSchema() {
        // Need to use new key value to avoid collision with other create actions
        EndpointTestBuilder.from(buildBlobCreateCase("newestfile.txt")).doPut(buildValidBlobUpdateRequestAsJson())
                .andCheckSchema().run();
    }

    @Test
    void testPut_failure_create_conflict() {
        EndpointTestBuilder.from(CREATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testPut_failure_create_conflict_validateResponseFormat() {
        EndpointTestBuilder.from(CREATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).andCheckFormat().run();
    }

    @Test
    void testPut_failure_create_conflict_validateSchema() {
        EndpointTestBuilder.from(CREATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).andCheckSchema().run();
    }

    void testPut_success_update() {
        EndpointTestBuilder.from(UPDATE_SUCCESS).doPut(buildValidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testPut_success_update_validateSchema() {
        EndpointTestBuilder.from(UPDATE_SUCCESS).doPut(buildValidBlobUpdateRequestAsJson()).andCheckSchema().run();
    }

    @Test
    void testPut_failure_update_conflict() {
        EndpointTestBuilder.from(UPDATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testPut_failure_update_conflict_validateResponseFormat() {
        EndpointTestBuilder.from(UPDATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).andCheckFormat().run();
    }

    @Test
    void testPut_failure_update_conflict_validateSchema() {
        EndpointTestBuilder.from(UPDATE_CONFLICT).doPut(buildValidBlobUpdateRequestAsJson()).andCheckSchema().run();
    }

    @Test
    void testPut_failure_badAuth() {
        EndpointTestBuilder.from(
                buildInvalidAuthCase(FULL_URL, new String[] { VALID_ECLIPSE_APP_TOKEN, VALID_ECLIPSE_BLOB_KEY })).doPut(
                        buildValidBlobUpdateRequestAsJson())
                .run();
    }

    @Test
    void testPut_failure_invalidUserAgent() {
        EndpointTestBuilder.from(buildWriteInvalidUserAgentCase(FULL_URL)).doPut(buildValidBlobUpdateRequestAsJson())
                .run();
    }

    @Test
    void testPut_failure_badRequest_invalidAppToken() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doPut(buildValidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testPut_failure_badRequest_invalidAppToken_validateSchema() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doPut(buildValidBlobUpdateRequestAsJson())
                .andCheckSchema().run();
    }

    @Test
    void testPut_failure_badRequest_invalidAppToken_validateResponseFormat() {
        EndpointTestBuilder.from(WRITE_SINGLE_INVALID_APP_TOKEN).doPut(
                buildValidBlobUpdateRequestAsJson()).andCheckFormat().run();
    }

    @Test
    void testPut_failure_badRequest_invalidBlobValue() {
        EndpointTestBuilder.from(WRITE_SINGLE_BAD_REQUEST).doPut(buildInvalidBlobUpdateRequestAsJson()).run();
    }

    @Test
    void testPut_failure_badRequest_invalidBlobValue_validateSchema() {
        EndpointTestBuilder.from(WRITE_SINGLE_BAD_REQUEST).doPut(buildInvalidBlobUpdateRequestAsJson()).andCheckSchema()
                .run();
    }

    @Test
    void testPut_failure_badRequest_invalidBlobValue_validateResponseFormat() {
        EndpointTestBuilder.from(WRITE_SINGLE_BAD_REQUEST).doPut(buildInvalidBlobUpdateRequestAsJson()).andCheckFormat()
                .run();
    }

    /**
     * Builds a 401 test case for a request with an invalid 'Authorization' header,
     * but a valid 'User-Agent' header.
     * 
     * @param URL    The target URL
     * @param params The URL params
     * @return The constructed EndpointTestCase
     */
    private EndpointTestCase buildInvalidAuthCase(String URL, String[] params) {
        return TestCaseHelper.prepareTestCase(URL, params, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
                .setHeaderParams(Optional.of(Map.of(HttpHeaderNames.AUTHORIZATION,
                        "Bearer invalid", HttpHeaderNames.USER_AGENT, "validAgent")))
                .setStatusCode(401).build();
    }

    /**
     * Builds a 403 test case for a request with a valid read token, but invalid
     * User-Agent header.
     * 
     * @param URL    The target URL
     * @param params The URL params
     * @return The constructed EndpointTestCase
     */
    private EndpointTestCase buildReadInvalidUserAgentCase(String URL, String[] params) {
        return TestCaseHelper.prepareTestCase(URL, params, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
                .setHeaderParams(
                        Optional.of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validRead",
                                HttpHeaderNames.USER_AGENT, "invalid")))
                .setStatusCode(403).build();
    }

    /**
     * Builds a 403 test case for a request with with a valid write token, but
     * invalid User-Agent header.
     * 
     * @param URL    The target URL
     * @param params The URL params
     * @return The constructed EndpointTestCase
     */
    private EndpointTestCase buildWriteInvalidUserAgentCase(String URL) {
        return TestCaseHelper
                .prepareTestCase(URL, new String[] { VALID_TEST_APP_TOKEN, VALID_TEST_BLOB_KEY },
                        SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
                .setHeaderParams(Optional
                        .of(Map.of(HttpHeaderNames.AUTHORIZATION, "Bearer validWrite", HttpHeaderNames.USER_AGENT,
                                "invalid")))
                .setStatusCode(403).build();
    }

    /**
     * Builds a 201 test case for a creation request using the desired blob key.
     * 
     * @param fileName The blob key/file name
     * @return The constructed EndpointTestCase
     */
    private EndpointTestCase buildBlobCreateCase(String fileName) {
        return TestCaseHelper
                .prepareTestCase(FULL_URL, new String[] { VALID_TEST_APP_TOKEN, fileName },
                        SchemaNamespaceHelper.BLOB_URL_SCHEMA_PATH)
                .setStatusCode(201)
                .setHeaderParams(validWriteWithAgent).build();
    }

    /**
     * Builds a valid mock BlobUpdateRequest entity and converts it to a json
     * string.
     * 
     * @return The mock valid BlobUpdateRequest as a Json string
     */
    private String buildValidBlobUpdateRequestAsJson() {
        try {
            String value = Base64.encode("testFileContents".getBytes());
            return objectMapper.writeValueAsString(BlobUpdateRequest.builder().setValue(value).build());
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Builds an invalid mock BlobUpdateRequest entity and converts it to a json
     * string.
     * 
     * @return The invalid mock BlobUpdateRequest as a Json string
     */
    private String buildInvalidBlobUpdateRequestAsJson() {
        try {
            return objectMapper
                    .writeValueAsString(BlobUpdateRequest.builder().setValue("invalid base64 string").build());
        } catch (Exception e) {
            return "";
        }
    }
}