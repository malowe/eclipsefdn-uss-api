/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.test.helpers;

public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String BLOBS_SCHEMA_PATH = BASE_SCHEMA_PATH + "blobs" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String BLOB_SCHEMA_PATH = BASE_SCHEMA_PATH + "blob" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String BLOB_UPDATE_REQUEST_SCHEMA_PATH = BASE_SCHEMA_PATH + "blob-update-request"
            + BASE_SCHEMA_PATH_SUFFIX;
    public static final String BLOB_URL_SCHEMA_PATH = BASE_SCHEMA_PATH + "blob-url"
            + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMA_PATH + "error" + BASE_SCHEMA_PATH_SUFFIX;
}
