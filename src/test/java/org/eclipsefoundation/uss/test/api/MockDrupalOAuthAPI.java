/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.uss.test.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.efservices.api.DrupalOAuthAPI;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class MockDrupalOAuthAPI implements DrupalOAuthAPI {

    List<DrupalOAuthData> tokens;
    List<DrupalUserInfo> users;

    public MockDrupalOAuthAPI() {
        tokens = new ArrayList<>();
        tokens.addAll(Arrays.asList(
                DrupalOAuthData.builder()
                        .setAccessToken("validRead")
                        .setClientId("test-id")
                        .setUserId("42")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("read")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("validWrite")
                        .setClientId("test-id")
                        .setUserId("42")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("write")
                        .build(),
                DrupalOAuthData.builder()
                        .setAccessToken("invalidScope")
                        .setClientId("test-id")
                        .setExpires(Instant.now().getEpochSecond() + 20000)
                        .setScope("blob")
                        .build()));

        users = new ArrayList<>();
        users.addAll(Arrays.asList(
                DrupalUserInfo.builder()
                        .setSub("42")
                        .setName("fakeuser")
                        .setGithubHandle("fakeuser")
                        .build(),
                DrupalUserInfo.builder()
                        .setSub("333")
                        .setName("otheruser")
                        .setGithubHandle("other")
                        .build()));
    }

    @Override
    public DrupalOAuthData getTokenInfo(String token) {
        return tokens.stream().filter(t -> t.getAccessToken().equalsIgnoreCase(token)).findFirst().orElse(null);
    }

    @Override
    public DrupalUserInfo getUserInfoFromToken(String token) {
        DrupalOAuthData tokenInfo = getTokenInfo(DrupalAuthHelper.stripBearerToken(token));
        if (tokenInfo == null || tokenInfo.getUserId() == null) {
            throw new FinalForbiddenException("The access token provided is invalid");
        }

        return users.stream().filter(u -> u.getSub().equalsIgnoreCase(tokenInfo.getUserId())).findFirst()
                .orElse(null);
    }
}