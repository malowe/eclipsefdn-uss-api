
-- eclipse_api.api_eclipse_api_blob

CREATE TABLE `api_eclipse_api_blob` (
  `bid` SERIAL NOT NULL,
  `uid` int NOT NULL DEFAULT 0,
  `blob_key` varchar(255) NOT NULL,
  `application_token` varchar(255) NOT NULL,
  `blob_value` BLOB NOT NULL,
  `etag` varchar(255) NOT NULL,
  `created` int NOT NULL DEFAULT 0,
  `changed` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`bid`)
);

INSERT INTO `api_eclipse_api_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'testKey', 'testToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'testTag', 1684509568, 1684509568);
INSERT INTO `api_eclipse_api_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'filename.txt', 'testToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'etag', 1684509568, 1684509568);
INSERT INTO `api_eclipse_api_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'eclipseKey', 'eclipseToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'eTaG30Test', 1684509568, 1684509568);
INSERT INTO `api_eclipse_api_blob` (`uid`, `blob_key`, `application_token`, `blob_value`, `etag`, `created`, `changed`)
  VALUES (42, 'application.properties', 'eclipseToken', 'c2FtcGxlIGRvY3VtZW50IGNvbnRlbnRz', 'appTagTest', 1684509568, 1684509568);


-- eclipse_api.api_eclipse_api_blob_application_usage

CREATE TABLE `api_eclipse_api_blob_application_usage` (
  `id` SERIAL NOT NULL,
  `uid` int NOT NULL DEFAULT 0,
  `blob_key` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `application_token` varchar(255) NOT NULL,
  `created` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);