# eclipsefdn-uss-api

This API manages the USS operations.

## Getting started

### Requirements

* Docker
* mvn
* make
* yarn
* Java 11 >

### Setup

Additional setup instructions will be added as the project develops.

#### Build and Start Server

```bash
make compile-start
```

#### Live-coding Dev Mode

```bash
make dev-start
```

#### Generate Spec

```bash
make compile-test-resources
```

#### Running Tests

```bash
mvn test
```

#### Render a Live UI Preview of the API Spec

```bash
make start-spec
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-uss-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-uss-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-uss-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
<http://www.eclipse.org/legal/epl-2.0>.

SPDX-License-Identifier: EPL-2.0
